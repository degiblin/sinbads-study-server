webpackJsonp([0,3],{

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutPageComponent = (function () {
    function AboutPageComponent() {
    }
    AboutPageComponent.prototype.ngOnInit = function () {
    };
    return AboutPageComponent;
}());
AboutPageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
        template: __webpack_require__(666),
        styles: [__webpack_require__(662)]
    }),
    __metadata("design:paramtypes", [])
], AboutPageComponent);

//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/about-page.component.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResumePageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ResumePageComponent = (function () {
    function ResumePageComponent() {
    }
    ResumePageComponent.prototype.ngOnInit = function () {
    };
    return ResumePageComponent;
}());
ResumePageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
        template: __webpack_require__(669),
        styles: [__webpack_require__(665)]
    }),
    __metadata("design:paramtypes", [])
], ResumePageComponent);

//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/resume-page.component.js.map

/***/ }),

/***/ 385:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 385;


/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(505);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/main.js.map

/***/ }),

/***/ 503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(492);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_page_about_page_component__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__resume_page_resume_page_component__ = __webpack_require__(331);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_2__about_page_about_page_component__["a" /* AboutPageComponent */] },
    { path: 'resume', component: __WEBPACK_IMPORTED_MODULE_3__resume_page_resume_page_component__["a" /* ResumePageComponent */] },
    { path: '', redirectTo: '/about', pathMatch: 'full' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]],
    })
], AppRoutingModule);

//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/app-routing.module.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Dale Giblin';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(667),
        styles: [__webpack_require__(663)]
    })
], AppComponent);

//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/app.component.js.map

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_routing_module__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__about_page_about_page_component__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__resume_page_resume_page_component__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__contact_form_contact_form_component__ = __webpack_require__(506);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__about_page_about_page_component__["a" /* AboutPageComponent */],
            __WEBPACK_IMPORTED_MODULE_7__resume_page_resume_page_component__["a" /* ResumePageComponent */],
            __WEBPACK_IMPORTED_MODULE_8__contact_form_contact_form_component__["a" /* ContactFormComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__app_routing_module__["a" /* AppRoutingModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/app.module.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(673);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactFormComponent = (function () {
    function ContactFormComponent(http) {
        this.http = http;
        this.successAlert = false;
        this.failAlert = false;
    }
    ContactFormComponent.prototype.ngOnInit = function () {
    };
    ContactFormComponent.prototype.submitMessage = function () {
        var _this = this;
        this.http.post('api/contact', { email: this.email, message: this.message })
            .toPromise()
            .then(function (res) {
            _this.email = null;
            _this.message = null;
            _this.successAlert = true;
        })
            .catch(function (err) {
        });
    };
    ContactFormComponent.prototype.closeAlert = function (alert) {
        switch (alert) {
            case "success":
                {
                    this.successAlert = false;
                    break;
                }
            case "fail":
                {
                    this.failAlert = false;
                    break;
                }
            default:
                break;
        }
    };
    return ContactFormComponent;
}());
ContactFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_4" /* Component */])({
        selector: 'contact-form',
        template: __webpack_require__(668),
        styles: [__webpack_require__(664)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], ContactFormComponent);

var _a;
//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/contact-form.component.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/environment.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__ = __webpack_require__(516);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__ = __webpack_require__(521);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__ = __webpack_require__(523);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__ = __webpack_require__(686);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__);
// This file includes polyfills needed by Angular and is loaded before
// the app. You can add your own extra polyfills to this file.
















//# sourceMappingURL=D:/Projects/WebSites/sinbads-study/src/polyfills.js.map

/***/ }),

/***/ 662:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 663:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 664:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 665:
/***/ (function(module, exports) {

module.exports = ".pad{\r\n    padding-bottom: 50px;\r\n}"

/***/ }),

/***/ 666:
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron\">\r\n  <h1>\r\n    Hi! <small>My name is Dale Giblin</small>\r\n  </h1>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"well\" id=\"about\">\r\n      <h1>About Me</h1>\r\n      <p>I am a front-end web developer living in Mooresville, Indiana.</p>\r\n      <p>I love programming and almost always have new projects running through my mind.</p>\r\n      <p>My framework of choice is Angular, though I have used ASP.NET and Django in the past.</p>\r\n      <p>Other technologies I am familiar with are listed on my Resume page.</p>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"well\" id=\"contact\">\r\n      <h1>Contact Me</h1>\r\n      <contact-form></contact-form>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 667:
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default\">\r\n  <div class=\"container\">\r\n    <!-- Brand and toggle get grouped for better mobile display -->\r\n    <div class=\"navbar-header\">\r\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\"\r\n        aria-expanded=\"false\">\r\n        <span class=\"sr-only\">Toggle navigation</span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n        <span class=\"icon-bar\"></span>\r\n        </button>\r\n        <a class=\"navbar-brand\" href=\"#\">{{title}}</a>\r\n    </div>\r\n    <!-- Collect the nav links, forms, and other content for toggling -->\r\n    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n      <ul class=\"nav navbar-nav\">\r\n        <li><a routerLink=\"/about\">About</a></li>\r\n        <li><a routerLink=\"/resume\">Resume</a></li>\r\n        <li class=\"dropdown\">\r\n          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Projects<span class=\"caret\"></span></a>\r\n          <ul class=\"dropdown-menu\">\r\n            <li class=\"disabled\"><a routerLink=\"/random\">Random Generators</a></li>\r\n            <li class=\"disabled\"><a routerLink=\"/stocks\">Stocks</a></li>\r\n          </ul>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n<div class=\"container\">\r\n  <router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ 668:
/***/ (function(module, exports) {

module.exports = "<div class=\"alert alert-success\" role=\"alert\" *ngIf=\"successAlert\">Success!\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"closeAlert('success')\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n</div>\r\n<div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"failAlert\">Failed!\r\n  <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"closeAlert('fail')\">\r\n    <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n</div>\r\n<form (ngSubmit)=\"submitMessage()\">\r\n  <div class=\"form-group\">\r\n    <label for=\"#emailInput\">Email</label>\r\n    <input type=\"email\" class=\"form-control\" placeholder=\"Email\" id=\"emailInput\" name=\"email\" required [(ngModel)]=\"email\">\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label for=\"#messageInput\">Message</label>\r\n    <textarea class=\"form-control\" placeholder=\"Message\" id=\"messageInput\" name=\"message\" style=\"resize: vertical\" required [(ngModel)]=\"message\">\r\n    </textarea>\r\n  </div>\r\n  <button type=\"submit\" class=\"btn btn-primary\">Send</button>\r\n</form>"

/***/ }),

/***/ 669:
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron\">\r\n  <h1>\r\n    Resume\r\n  </h1>\r\n</div>\r\n<div class=\"well well-lg\" id=\"skills\">\r\n  <div class=\"row\">\r\n    <h1 class=\"col-md-4\">Skills</h1>\r\n    <div class=\"col-md-4\">\r\n      <h3 class=\"text-center\">Comfortable With</h3>\r\n      <ul>\r\n        <li>HTML/CSS/Javascript</li>\r\n        <li>Angular</li>\r\n        <li>NodeJS</li>\r\n        <li>Express</li>\r\n        <li>MongoDB</li>\r\n        <li>Jasmine BDD Framework</li>\r\n        <li>Sass</li>\r\n        <li>Typescript</li>\r\n        <li>Git</li>\r\n      </ul>\r\n    </div>\r\n    <div class=\"col-md-4\">\r\n      <h3 class=\"text-center\">Past Experience</h3>\r\n      <ul>\r\n        <li>JQuery</li>\r\n        <li>QUnit Testing Framework</li>\r\n        <li>Python</li>\r\n        <li>Django</li>\r\n        <li>C#</li>\r\n        <li>ASP.NET Core</li>\r\n        <li>NUnit Testing Framework</li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"well well-lg\" id=\"education\">\r\n  <div class=\"row\">\r\n    <h1 class=\"col-md-4\">Education</h1>\r\n    <div class=\"col-md-8\">\r\n      <h2>Master of Science, Computer Science</h2>\r\n      <h3>Applied Computer Science</h3>\r\n      <h4 class=\"pad\">East Tennessee State University <small>Johnson City, TN, 2013-2015</small></h4>\r\n\r\n      <h2>Bachelor of Science</h2>\r\n      <h3>Computer Information Systems and Mathematics <small>Double Major</small></h3>\r\n      <h4>Milligan College <small>Milligan College, TN, 2009-2013</small></h4>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"well well-lg\" id=\"experience\">\r\n  <div class=\"row\">\r\n    <h1 class=\"col-md-4\">Experience</h1>\r\n    <div class=\"col-md-8\">\r\n      <h2>Adjunct Professor</h2>\r\n      <h4>Milligan College <small>Milligan College, TN, 2016-Present</small></h4>\r\n      <p class=\"pad\">I am currently an adjunct professor for the computer department at Milligan College.</p>\r\n\r\n      <h2>Graduate Assisstant</h2>\r\n      <h4>East Tennessee State University, ITS Department <small>Johnson City, TN, 2013-2015</small></h4>\r\n      <p class=\"pad\">I assisted the Network Support Specialist in the day-to-day maintenance of the university’s LAN. This involved the\r\n        installation of Cisco switches and Wireless Access Points, troubleshooting any problems faculty and staff had connecting\r\n        to the network, assisting in the installation and repair of ethernet wall jacks, and connecting them to the network.</p>\r\n\r\n      <h2>Lead Xamarin Programmer</h2>\r\n      <h4>East Tennessee State University, Graduate Capstone <small>Johnson City, TN, 2014-2015</small></h4>\r\n      <p>I led a team in the design and implementation of an iOS application for the National Storytelling Festival. The goal\r\n        of the project was to complement the Festival’s paper programs with an iOS version.</p>\r\n      <p>We used Xamarin Forms and C# to build the application. At the time, it was still being developed, and we had to work\r\n        around bugs to get it to do what we wanted. We attempted to use an Agile environment for development, but it was\r\n        a semester and a half before we got it working. Git was our first VCS, but we later changed to Mercurial. We attempted\r\n        a Test Driven Development approach using NUnit, but this proved to be impractical as most of our code was integrated\r\n        with Xamarin. So, we dropped the Driven Development part and just wrote tests where we could.</p>\r\n      <p class=\"pad\">I did most of the most of work deploying the app to the Apple App Store and then sent the updates as well.</p>\r\n\r\n      <h2>Internship</h2>\r\n      <h4>Milligan College <small>Milligan College, TN, 2012-2013</small></h4>\r\n      <p>I designed and built a mobile application for Milligan College using Appcelerator. Their Titanium SDK allowed me to\r\n        easily design the app for both iOS and Android platforms. The project was a proof of concept to determine if the\r\n        college wanted to pursue further development. This meant I was the only developer on the project and that I had a\r\n        lot of freedom in the design. The design process was Agile, though only by accident since I didn’t know about Agile\r\n        at that time. I would show my work to the “customers”, get feedback, make changes, and repeat.</p>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 687:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(386);


/***/ })

},[687]);
//# sourceMappingURL=main.bundle.map