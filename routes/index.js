var express = require('express');
var router = express.Router();
var api = require('./api/api');

/* GET home page. */

router.get('/', function(req, res) {
  res.sendFile('index.html');
});

router.use('/api', api);

module.exports = router;
