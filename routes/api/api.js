const api = require('express').Router();
const contact = require('./contact');

api.use('/contact', contact);
api.use('/', (req, res, next) => {
    res.send('api works!');
});

module.exports = api;